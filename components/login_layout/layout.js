import styles from './layout.module.css'
import {Container} from 'react-bootstrap'

function Layout( { children } ){
    return (
        <Container fluid >
            {children}
        </Container>
    ) 
}
export default Layout;
