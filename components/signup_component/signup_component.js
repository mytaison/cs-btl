import Link from 'next/link'
import styles from './signup_component.module.css'
import { Button, Card , Form } from 'react-bootstrap'
export default function SignupComponent() {
    return (
        <div className={styles.loginCard}>
            <div className={styles.logoArea}>
                <div id={styles.logoImg}><img src="/cloudshrink-temp.png"></img></div>
                <div id={styles.logoText}>cloudshrink</div>
            </div>
            <Card className={styles.card}>
                <Card.Body>
                    <Card.Title className={styles.cardTitle}>A step away from saving your cloud</Card.Title>
                    {/* <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle> */}
                    <Form>
                        <Form.Group controlId="formBasicFirstName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="email" placeholder="Enter first name" />
                        </Form.Group>
                        <Form.Group controlId="formBasicLastName">
                            <Form.Label>First Name</Form.Label>
                            <Form.Control type="email" placeholder="Enter last name" />
                        </Form.Group>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                            {/* <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text> */}
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Re-type Password</Form.Label>
                            <Form.Control type="password" placeholder="Re-type password" />
                        </Form.Group>
                        <div className={styles.submitButtonDiv}>
                            <Button variant="primary" type="submit" align="center">
                                Sign up
                            </Button>
                        </div>
                    </Form>
                    <hr></hr>
                    <div className={styles.submitButtonDiv}>
                        <Link href="/" passHref><Card.Link>Already a user? Log in</Card.Link></Link>
                    </div>
                    
                </Card.Body>
            </Card>
        </div>
  )
}