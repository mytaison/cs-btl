import styles from './dashboard_navbar_component.module.css'
import {Button, Form, FormControl, Navbar, Nav, NavDropdown} from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch , faBell } from '@fortawesome/free-solid-svg-icons'
export default function navbar(props){
    return(
        <>
        <Navbar bg="light" expand="lg" className={styles.navbar}>
            <Navbar.Brand href="#home" className={styles.navbrand}>
                <img
                    alt="logo"
                    src="/cloudshrink-temp.png"
                    width="60"
                    height="46"
                    className="d-inline-block align-top"
                />{' '}
                cloudshrink
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav" className={styles.navbar_navsarea}>
                <Nav className="mr-auto" className={styles.navbar_navs}>
                    <Nav.Link href="#dashboard" className={styles.navbar_navs_link}>Dashboard</Nav.Link>
                    <Nav.Link href="#integration" className={styles.navbar_navs_link}>Integration</Nav.Link>
                    <Nav.Link href="#setting" className={styles.navbar_navs_link}>Setting</Nav.Link>
                </Nav>
                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-dark" className={styles.navIconBtn}><FontAwesomeIcon icon={faSearch} size="1x"/></Button>
                    <Button variant="outline-dark" className={styles.navIconBtn}><FontAwesomeIcon icon={faBell} size="1x"/></Button>
                    <NavDropdown className={styles.userNavLink} title={
                        <div className={styles.userNavDropdown}>
                            <div className={styles.userNavDropdownImgDiv}>
                                <img className="thumbnail-image" 
                                src="/user_image.png" 
                                alt="user pic"
                                />
                            </div>
                            <div className={styles.userNavDropdownNameDiv}>
                                Fariz 
                            </div>
                        </div>
                    }  id="basic-nav-dropdown">
                        <NavDropdown.Item className={styles.userNavDropdownNameItem}>Fariz</NavDropdown.Item>
                        <NavDropdown.Item href="#">Logout</NavDropdown.Item>
                    </NavDropdown>
                </Form>
            </Navbar.Collapse>
        </Navbar>
        </>
    );
}