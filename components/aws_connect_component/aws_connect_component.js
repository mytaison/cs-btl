import styles from './aws_connect_component.module.css'
import {Button, Modal} from 'react-bootstrap'
export default function aws_connect_component(props){
    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>Connect with AWS</Modal.Title>
            </Modal.Header>
            <Modal.Body>Enter your details</Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={props.handleClose}>
                Close
            </Button>
            <Button variant="primary" onClick={props.handleClose}>
                Connect
            </Button>
            </Modal.Footer>
        </Modal>
    )
}