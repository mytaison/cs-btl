import React , { useState} from 'react'
import styles from './connect_service_component.module.css'
import { Card } from 'react-bootstrap'
// importing modals
import AwsConnectModal from '../aws_connect_component/aws_connect_component'
import GcpConnectModal from '../gcp_connect_component/gcp_connect_component'
import AzureConnectModal from '../azure_connect_component/azure_connect_component'


export default function connect_service_component(props){
    // Modal Controls
    const [showAws, setShowAws] = useState(false);
    const [showGcp, setShowGcp] = useState(false);
    const [showAzure, setShowAzure] = useState(false);
    const handleCloseAws = () => setShowAws(false);
    const handleShowAws = () => setShowAws(true);
    const handleCloseGcp = () => setShowGcp(false);
    const handleShowGcp = () => setShowGcp(true);
    const handleCloseAzure = () => setShowAzure(false);
    const handleShowAzure = () => setShowAzure(true);

    return (
        <div className={styles.cardHolder}>
            <div className={styles.connectServiceHeader}>{props.header}</div>
            <div className={styles.connectServiceCardsGroup}>
                <Card className={styles.cloudServiceCard} onClick={handleShowAws}>
                    <Card.Body>
                        <div className={styles.imgDiv}>
                            <img src="./aws.png"></img>
                        </div>
                        <Card.Title className={styles.cardTitle}>Connect AWS</Card.Title>
                    </Card.Body>
                </Card>
                <Card className={styles.cloudServiceCard} onClick={handleShowAzure}>
                    <Card.Body>
                        <div className={styles.imgDiv}>
                            <img src="./azure.png"></img>
                        </div>
                        <Card.Title className={styles.cardTitle}>Connect Azure</Card.Title>
                    </Card.Body>
                </Card>
                <Card className={styles.cloudServiceCard} onClick={handleShowGcp}>
                    <Card.Body>
                        <div className={styles.imgDiv}>
                            <img src="./gcp.png"></img>
                        </div>
                        <Card.Title className={styles.cardTitle}>Connect G. Cloud</Card.Title>
                    </Card.Body>
                </Card>
                <Card className={styles.cloudServiceCard}>
                    <Card.Body>
                        <div className={styles.imgDiv}>
                            <img src="./alibaba.png"></img>
                        </div>
                        <Card.Title className={styles.cardTitle}>Connect A. Cloud</Card.Title>
                    </Card.Body>
                </Card>
            </div>
            <AwsConnectModal  show={showAws} handleClose={handleCloseAws} ></AwsConnectModal>
            <GcpConnectModal  show={showGcp} handleClose={handleCloseGcp} ></GcpConnectModal>
            <AzureConnectModal  show={showAzure} handleClose={handleCloseAzure} ></AzureConnectModal>
        </div>
    )
}