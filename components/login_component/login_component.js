import Link from 'next/link'
import styles from './login_component.module.css'
import { Button, Card , Form } from 'react-bootstrap'
export default function LoginComponent() {
    
    return (
        <div className={styles.loginCard}>
            <div className={styles.logoArea}>
                <div id={styles.logoImg}><img src="/cloudshrink-temp.png"></img></div>
                <div id={styles.logoText}>cloudshrink</div>
            </div>
            <Card className={styles.card}>
                <Card.Body>
                    <Card.Title className={styles.cardTitle}>Let's get saving</Card.Title>
                    {/* <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle> */}
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" />
                            {/* <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                            </Form.Text> */}
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" />
                        </Form.Group>
                        <div className={styles.submitButtonDiv}>
                            <Link href="/dashboard" passHref>
                                <Button variant="primary" type="submit" align="center">
                                    Log in
                                </Button>
                            </Link>
                        </div>
                    </Form>
                    <hr></hr>
                    <div className={styles.submitButtonDiv}>
                        <Link href="" passHref><Card.Link>Can't Login ?</Card.Link></Link>
                        <Link href="/signup" passHref><Card.Link >Signup for new user?</Card.Link></Link>
                    </div>
                    
                </Card.Body>
            </Card>
        </div>    
  )
}
