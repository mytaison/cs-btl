import Link from 'next/link';
import Head from 'next/head';
import Layout from '../../components/login_layout/layout';
import { Button } from 'react-bootstrap';

export default function FirstPost(){
    return (
    <>
        <Layout>
            <Head>
                <title>My First Post</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <h1>This is my first post</h1>
            <h2>
                <Link href="/">
                    <Button variant="primary">Back to Home</Button>
                    {/* <a>Back to Home</a> */}
                </Link>
            </h2>
        </Layout>
    </>)
}