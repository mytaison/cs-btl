import Head from 'next/head'
import DashbaordLayout from '../components/dashboard_layout/layout'
import DashboardNavbar from '../components/dashboard_navbar_component/dashboard_navbar_component'
import ConnectServiceComponent from '../components/connect_service_component/connect_service_component'

export default function Home() {
    return (
    <>
      <Head>
        <title>cloudshrink - Dashboard</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <DashbaordLayout>
        <DashboardNavbar></DashboardNavbar>
        <div className="contentHolder">
          <ConnectServiceComponent header="Connect a platform to get started"></ConnectServiceComponent>
        </div>
      </DashbaordLayout>
      <style jsx>
        {`
        .contentHolder{
          display: flex;
          min-height: calc(100vh - 69px);
          align-items: center;
          justify-content: center;
        }
        `}  
      </style>
    </>
    )
}