import Head from 'next/head'
import LoginLayout from '../components/login_layout/layout'
import SignupComponent from '../components/signup_component/signup_component'
export default function Home() {
    return (
    <>
      <Head>
        <title>cloudshrink - Sign Up</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <LoginLayout>
        <SignupComponent></SignupComponent>
      </LoginLayout>

    </>
    )
}