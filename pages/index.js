import Head from 'next/head'
import Link from 'next/link'
import LoginComponent from '../components/login_component/login_component'
import LoginLayout from '../components/login_layout/layout'

export default function Home() {
  return (
    <>
      <Head>
        <title>cloudshrink</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <LoginLayout>
        <LoginComponent></LoginComponent>
      </LoginLayout>

    </>
  )
}
