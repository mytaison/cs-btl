const hostname = 'localhost';
const port = 2000;
module.exports = {
    env: {
        HOSTNAME: hostname ,
        PORT: port,
        HOST: `http://${hostname}:${port}`
    },
    devIndicators: {
        autoPrerender: false,
    },
    poweredByHeader: false,

  }